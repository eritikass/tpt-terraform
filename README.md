# infra as code

 - install [terraform](https://www.terraform.io/)
 - install [aws cli](https://aws.amazon.com/cli/)

configure aws cli

 - open service in AWS named "IAM"
 - find your user

```bash
# when you use school computer
export AWS_ACCESS_KEY_ID=<KEYID>
export AWS_SECRET_ACCESS_KEY=<secret>
export AWS_SESSION_TOKEN=<token>

# if personal one then run
aws configure
# and enter your info
# config is stored in
# -  ~/.aws/credentials
# -  ~/.aws/config
```

## use terraform


```
# init terraform
terraform init

# apply changes
terraform apply
# write yes to accept

# remove everything once done
terraform destroy
```